var path = require('path');
var precss = require('precss');
var autoprefixer = require('autoprefixer');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var webpack = require('webpack');
var htmlConfig = new HtmlWebpackPlugin({
	template: __dirname + '/src/index.html',
	filename: 'index.html',
	inject: 'body'
});
module.exports = {
	entry: './src/main.js',
	output: {
		path: __dirname + '/dist',
		filename: 'bundle.js'
	},
	devServer: {
		historyApiFallback: true,
		port: 5050,
		stats: {
			colors: true,
			hash: false,
			version: false,
			timings: false,
			assets: false,
			chunks: false,
			modules: false,
			reasons: false,
			children: false,
			source: false,
			errors: true,
			errorDetails: false,
			warnings: false,
			publicPath: true
		}
	},
	module: {
		loaders: [
			{
				test: /\.(js|jsx)$/,
				loader: 'babel-loader',
				exclude: /node_modules/,
				query: {
					presets: ['es2015', 'react', 'stage-2']
				}
			}, {
				test: /\.css$/,
				loader: ExtractTextPlugin.extract('style-loader', ['css-loader', 'postcss-loader', 'sass-loader'])
			}, {
				test: /\.scss$/,
				loaders: ['style', 'css', 'sass']
			}, {
				test: /\.(png|jpg|jpeg|gif|woff)$/,
				loader: 'url-loader?limit=8192'
			}
		]
	},
	eslint: {
		configFile: './.eslintrc'
	},
	plugins: [
		htmlConfig, new ExtractTextPlugin('style.css'), new webpack.DefinePlugin({
			GA_TRACKING_CODE: JSON.stringify('UA-81181314-1')
		})
	],
	resolve: {
		extensions: [
			'', '.js', '.jsx'
		],
		modulesDirectories: [
			'node_modules', 'src/assets/img', 'src/sass/', 'src/js/'
		],
		root: path.resolve(__dirname),
		alias: {
			components: __dirname + '/src/js/components',
			animations: __dirname + '/src/js/animations',
			config: __dirname + '/src/js/config',
			util: __dirname + '/src/js/util',
			svg: __dirname + '/src/svg'
		}
	},
	postcss: function() {
		return [precss, autoprefixer];
	}
};