import ga from 'ga-react-router'
import {browserHistory} from 'react-router';
const analyticsListenr = {
	unlisten: browserHistory.listen(location => {
		ga('send', location);
	})
}
export default analyticsListenr;