/// CORE
import React, {PropTypes} from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
// Layout
import Nav from 'components/Nav';
import Header from 'components/Header';
import Loader from 'components/Loader';
import Logo from 'components/Logo';
import Symbols from 'components/Symbols';
// UTIL
import mainTimeline from 'animations/mainTimeline';
// App
class App extends React.Component {
	constructor(props) {
		super(props);
	}
	componentDidMount() {
		mainTimeline.runSequence('home');
	}
	render() {
		const {children, location} = this.props;
		return (
			<div ref="page">
				<Nav path={location.pathname}/>
				<Symbols/>
				<Loader/>
				<Header>
					<Logo/>
				</Header>
				<ReactCSSTransitionGroup component="div" transitionName="fade" transitionEnterTimeout={500} transitionLeaveTimeout={500}>
					{React.cloneElement(children, {key: location.pathname})}
				</ReactCSSTransitionGroup>
			</div>
		)
	}
}
App.propTypes = {};
export default App;