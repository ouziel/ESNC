import {TweenMax, TimelineMax} from 'gsap';
import mainTimeline from './mainTimeline';
import makeTimeline from './makeTimeline';
import config from 'animations/config';
import {refs} from 'config';
// Timeline Creators are responsible for generating
// an object with props to tween
function navTL(node) {
	const timeline = {
		name: config.labels.nav,
		data: [
			{
				func: 'to',
				node,
				dur: .5,
				props: {
					x: 0,
					opacity: 1,
					ease: Power4.easeOut,
					delay: 1,
					onComplete() {
						// 	const {header} = refs.data;
						// 	console.log(refs.data, refs, header);
						// 	const box = header.querySelector('#canvas_box');
						// 	TweenMax.to(box, .3, {
						// 		x: "+=50%",
						// 		opacity: 0
						// 	});
					}
				}
			}
		]
	}
	return makeTimeline(timeline);
}
export default navTL;