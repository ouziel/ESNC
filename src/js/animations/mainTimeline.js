import Orchs from 'util/Orchs';
import config from './config';
const mainTimeline = new Orchs();
mainTimeline.addSequence({
	name: 'home',
	timelines: [config.labels.loadingBar, config.labels.header, config.labels.slogan, config.labels.nav]
})
export default mainTimeline;