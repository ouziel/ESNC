const config = {
	labels: {
		header: 'header',
		loadingBar: 'loadingBar',
		nav: 'nav',
		slogan: 'slogan',
		banner: 'banner'
	}
}
export default config;