import {TimelineMax} from 'gsap';
import config from 'animations/config';
import {log} from 'util';
function makeTimeline({
	name,
	data,
	config = {
		paused: false
	}
} = {}) {
	if (data) {
		log(config);
		const timeline = new TimelineMax(config);
		// for (let i = 0; i < data.length; i++) {
		// 	let step = data[i];
		// 	const {func, node, dur, label, props} = step;
		// 	timeline[func](node, dur, props);
		// }
		// REVIEW: this seems alot slower..
		data.forEach(step => {
			const {
				func,
				node,
				dur,
				label,
				props,
				position
			} = step;
			timeline[func](node, dur, props, position);
		})
		return timeline;
	} else {
		throw new Error('missing prop {data} in timeline object');
	}
}
export default makeTimeline;