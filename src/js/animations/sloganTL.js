// Timeline Creators are responsible for generating
// an object with props to tween
////////////////////
// VENDORS
import {TweenMax, TimelineMax} from 'gsap';
import $ from 'jquery';
import DrawSVGPlugin from 'gsap/src/uncompressed/plugins/DrawSVGPlugin.js';
// UTIL
import mainTimeline from './mainTimeline';
import makeTimeline from './makeTimeline';
import config from 'animations/config';
import {colors, refs} from 'config';
/// styles
const styleFrom = {
	opacity: 0,
	fill: 'transparent',
	stroke: colors.black2,
	x: -5,
	'stroke-dasharray': 1150,
	'stroke-dashoffset': 1150
}
const styleTo = {
	opacity: 1,
	x: 0,
	fill: colors.black2,
	stroke: colors.black2,
	'stroke-dashoffset': 1000
}
/// timeline
function sloganTL(node) {
	const W = window.innerWidth,
		H = window.innerHeight;
	const text_we = node.querySelector('#text_we');
	const text_do = node.querySelector('#text_do');
	const span_we = node.querySelector('#span_we');
	const span_do = node.querySelector('#span_do');
	const char_e = node.querySelector('#char_e');
	const char_d = node.querySelector('#char_d');
	const timeline = {
		name: config.labels.slogan,
		data: [
			{
				func: 'to',
				node: [
					text_we, text_do
				],
				dur: 0,
				props: {
					attr: {},
					css: styleFrom,
					ease: Power1.easeIn,
					onComplete() {
						$(this.target).addClass('fill');
						$(node).addClass('animated');
					}
				}
			}, {
				func: 'to',
				node: text_we,
				dur: 1,
				props: {
					attr: {},
					css: styleTo,
					ease: Power1.easeIn,
					onComplete() {
						$(this.target)
						// .addClass('fill')
						// .addClass('do');
						const morphTL = refs.get('morphTL');
						morphTL.play();
					}
				}
			}, {
				func: 'to',
				node: text_do,
				dur: 1,
				position: "-=0.9",
				props: {
					attr: {},
					css: styleTo,
					ease: Power1.easeIn,
					delay: .5,
					onComplete() {
						const banner = refs.get('banner');
						$(banner).addClass('banner_on')
					}
				}
			}, {
				func: 'to',
				node: [
					text_we, text_do
				],
				dur: .2,
				position: "-=0.2",
				props: {
					css: {
						'stroke-dasharray': 3000,
						'stroke-dashoffset': 0
					},
					ease: Power1.easeIn
				}
			}
		]
	}
	return makeTimeline(timeline);
}
export default sloganTL;