import {TweenMax, TimelineMax} from 'gsap';
import mainTimeline from './mainTimeline';
import makeTimeline from './makeTimeline';
import config from 'animations/config';
// Timeline Creators are responsible for generating
// an object with props to tween
function headerTL(node) {
	const W = window.innerWidth,
		H = window.innerHeight;
	const timeline = {
		name: config.labels.header,
		data: [
			{
				func: 'to',
				node,
				dur: 1,
				props: {
					opacity: 1,
					ease: Power1.easeIn
				}
			}
		]
	}
	return makeTimeline(timeline);
}
export default headerTL;