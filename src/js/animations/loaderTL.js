import {TweenMax, TimelineMax} from 'gsap';
import mainTimeline from './mainTimeline';
import makeTimeline from './makeTimeline';
import config from 'animations/config';
// Timeline Creators are responsible for generating
// an object with props to tween
//
function loaderTL(loaderNode) {
	const W = window.innerWidth,
		H = window.innerHeight;
	const {bg, bar} = loaderNode.children;
	const timeline = {
		name: config.labels.loadingBar,
		data: [
			{
				func: 'to',
				node: bar,
				dur: 1.1,
				props: {
					scaleX: 1,
					ease: Power1.easeIn
				}
			}, {
				func: 'to',
				node: bg,
				dur: .2,
				props: {
					scaleY: 1,
					opacity: 1,
					ease: Linear.easeNone
				}
			}
		]
	}
	return makeTimeline(timeline);
}
export default loaderTL;