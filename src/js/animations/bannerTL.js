import {TweenMax, TimelineMax} from 'gsap';
import mainTimeline from './mainTimeline';
import makeTimeline from './makeTimeline';
import config from 'animations/config';
import MorphSVGPlugin from 'gsap/src/uncompressed/plugins/MorphSVGPlugin.js';
import morph from 'util/morph';
// Timeline Creators are responsible for generating
// an object with props to tween
//
//
//
//  not being used in this branch
//  efforts in /banner branch
function bannerTL(node, morphTL) {
	const {banner} = node;
	morphTL.stop();
	const timeline = {
		name: config.labels.banner,
		config: {
			onComplete() {
				morphTL.play();
			}
		},
		data: [
			{
				func: 'to',
				node: banner,
				dur: 0.01,
				props: {
					x: "-30px",
					opacity: 0,
					delay: 1
				},
				ease: Power1.easeIn
			}, {
				func: 'to',
				node: banner,
				dur: 1,
				props: {
					opacity: 1,
					x: 0
				},
				ease: Power1.easeIn
			}
		]
		// data: [
		// 	{
		// 		func: 'to',
		// 		node: banner,
		// 		dur: 0.01,
		// 		props: {
		// 			x: "-30px",
		// 			// opacity: 0
		// 		},
		// 		ease: Power1.easeIn
		// 	}, {
		// 		func: 'to',
		// 		node: banner,
		// 		dur: .7,
		// 		props: {
		// 			opacity: 1
		// 		},
		// 		ease: Power1.easeIn
		// 	}, {
		// 		func: 'to',
		// 		node: banner,
		// 		dur: 3,
		// 		position: "-=2",
		// 		props: {
		// 			x: 0
		// 		},
		// 		ease: SlowMo
		// 			.ease
		// 			.config(0.9, 0.7, false)
		// 	}, {
		// 		func: 'to',
		// 		node: website,
		// 		dur: 3,
		// 		delay: .2,
		// 		props: {
		// 			morphSVG: {
		// 				shape: webapps
		// 			},
		// 			ease: Power1.easeIn
		// 		}
		// 	}, {
		// 		func: 'to',
		// 		node: website,
		// 		dur: 3,
		// 		delay: .2,
		// 		props: {
		// 			morphSVG: {
		// 				shape: shops
		// 			},
		// 			ease: Power1.easeIn
		// 		}
		// 	}, {
		// 		func: 'to',
		// 		node: website,
		// 		dur: 5,
		// 		delay: .2,
		// 		props: {
		// 			morphSVG: {
		// 				shape: landing
		// 			},
		// 			ease: Power1.easeIn
		// 		}
		// 	}, {
		// 		func: 'to',
		// 		node: website,
		// 		dur: 3,
		// 		delay: .2,
		// 		props: {
		// 			morphSVG: {
		// 				shape: website
		// 			},
		// 			ease: Power1.easeIn
		// 		}
		// 	}
		// ]
	}
	return makeTimeline(timeline);
}
export default bannerTL