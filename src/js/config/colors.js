const colors = {
	black: '#0c0c0c',
	red: '#f44336',
	white: '#fff',
	yellow: '#FFE081',
	grey: '#bdbdbd',
	tan: '#d2b48c',
	cyan: '#03a9f4',
	black2: '#545454'
}
export default colors;