import fb from 'firebase'; //sdk
import firebase from './firebase'; //config
import refs from './refs';
import colors from './colors';
const production = false;
const config = {
	firebase,
	refs,
	production,
	colors
}
fb.initializeApp(config.firebase);
export default config;
export {refs, production, colors};