import {log} from 'util';
const refs = {
	data: {},
	update: (data) => Object.assign(refs.data, data),
	get: (name) => (refs.data[name])
		? refs.data[name]
		: log('no such ref', true),
	printAll: () => log(refs.data)
};
export default refs;