import React, {PropTypes} from 'react';
import ReactDOM, {render} from 'react-dom'
import {browserHistory, Router, Route, IndexRoute, Link} from 'react-router'
import App from './App';
import {Home, Contact, Index} from './pages';
// module is broken
// import analyticsListenr from './analytics';
render((
	<Router history={browserHistory}>
		<Route path="/" component={App}>
			<IndexRoute component={Home}/>
			<Route path="home" component={Home}/>
			<Route path="contact" component={Contact}/>
		</Route>
	</Router>
), document.getElementById('root'));