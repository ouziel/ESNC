import React, {PropTypes} from 'react';
import sloganTL from 'animations/sloganTL';
import mainTimeline from 'animations/mainTimeline';
import config from 'animations/config';
class Slogan extends React.Component {
	constructor(props) {
		super(props);
	}
	componentDidMount() {
		const slogan = this.refs.node;
		mainTimeline.addTimeline({name: config.labels.slogan, timeline: sloganTL(slogan)});
	}
	render() {
		return (
			<div className="slogan" ref="node">
				<span id="span_we">
					<svg >
						<text id="text_we" x="33%" y="50%" textAnchor="middle">
							<tspan>W</tspan>
							<tspan id="char_e">E</tspan>
						</text>
					</svg>
				</span>
				<span id="span_do">
					<svg >
						<text id="text_do" x="66%" y="50%" textAnchor="middle">
							<tspan id="char_d">D</tspan>
							<tspan>O</tspan>
						</text>
					</svg>
				</span>
			</div>
		)
	}
}
export default Slogan;