import React, {PropTypes} from 'react';
import {Link} from 'react-router';
import navTL from 'animations/navTL';
import mainTimeline from 'animations/mainTimeline';
import config from 'animations/config';
export default class Nav extends React.Component {
	constructor(props) {
		super(props);
	}
	componentDidMount() {
		const {node} = this.refs;
		mainTimeline.addTimeline({name: config.labels.nav, timeline: navTL(node)});
	}
	render() {
		const {path} = this.props;
		const cls = (path === '/')
			? 'link link-active'
			: 'link';
		return (
			<nav ref='node'>
				<ul>
					<li>
						<Link to='/contact' ref="contact" className="link" activeClassName="link-active">contact</Link>
					</li>
					<li>
						<Link to='/home' ref="home" className={cls} activeClassName="link-active">home</Link>
					</li>
				</ul>
			</nav>
		);
	}
}
Nav.propTypes = {};