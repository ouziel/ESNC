import React, {PropTypes} from 'react';
import {Link} from 'react-router';
export default class Logo extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		return (
			<div className="logo">
				<Link to='/home'>
					<svg>
						<text ref="logoChars" x="50%" y="60%" textAnchor="middle" vectorEffect="non-scaling-stroke">
							ESNC
						</text>
					</svg>
				</Link>
			</div>
		);
	}
}
Logo.propTypes = {};
{/*<tspan className="chars" ref="char_e1">
	E
</tspan>
<tspan className="chars" ref="char_s1">
	S
</tspan>
<tspan className="chars" ref="char_s2">
	S
</tspan>
<tspan className="chars" ref="char_e2">
	E
</tspan>
<tspan className="chars" ref="char_n">
	N
</tspan>
<tspan className="chars" ref="char_c">
	C
</tspan>
<tspan className="chars" ref="char_e3">
	E
</tspan>*/
}