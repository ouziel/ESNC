import React, {PropTypes} from 'react';
import headerTL from 'animations/headerTL';
import mainTimeline from 'animations/mainTimeline';
import img from 'svg/bg.jpg';
import loadPixelData from 'util/pixelData';
import {refs} from 'config';
export default class Header extends React.Component {
	constructor(props) {
		super(props);
	}
	componentDidMount() {
		const header = this.refs.header;
		mainTimeline.addTimeline({name: 'header', timeline: headerTL(header)});
		// loadPixelData(this.refs.canvas)
		refs.update({header})
	}
	render() {
		return (
			<header ref="header" {...this.props}>
				<div id="canvas_box" ref="canvas"></div>
				{this.props.children}
			</header>
		);
	}
}
Header.propTypes = {};