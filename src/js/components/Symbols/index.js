import React, {PropTypes} from 'react';
import {BoxesFilter, ZigiFilter} from 'util';
const ColorFilters = [
	{
		name: 'f_red',
		colors: [
			'#FFE081', '#f44336', '#000000'
		],
		opacity_1: 1,
		opacity_2: 1,
		opacity_3: 1,
		dx_1: 5,
		dx_2: -5,
		dx_3: 0,
		dy_1: 10,
		dy_2: 1,
		dy_3: 5
	}, {
		name: 'f_red2',
		colors: [
			'#FFE081', '#f44336', '#000000'
		],
		opacity_1: 1,
		opacity_2: 1,
		opacity_3: 1,
		dx_1: 3,
		dx_2: -3,
		dx_3: 0,
		dy_1: 7,
		dy_2: 1,
		dy_3: 3
	}, {
		name: 'f_cyan',
		colors: [
			'#f44336', '#03a9f4', '#000000'
		],
		opacity_1: 1,
		opacity_2: 1,
		opacity_3: 1,
		dx_1: 5,
		dx_2: -5,
		dx_3: 0,
		dy_1: 10,
		dy_2: 1,
		dy_3: 5
	}, {
		name: 'f_cyan2',
		colors: [
			'#f44336', '#03a9f4', '#000000'
		],
		opacity_1: 1,
		opacity_2: 1,
		opacity_3: 1,
		dx_1: 2,
		dx_2: 0,
		dx_3: 0,
		dy_1: 3,
		dy_2: 1,
		dy_3: 2
	}, {
		name: 'f_black',
		colors: [
			'#fff', '#fff', '#000000'
		],
		opacity_1: 0.5,
		opacity_2: 0,
		opacity_3: 0.5,
		dx_1: 5,
		dx_2: -5,
		dx_3: 0,
		dy_1: 10,
		dy_2: 1,
		dy_3: 5
	}, {
		name: 'f_blue',
		colors: [
			'#0000ff', '#0000ff', '#000000'
		],
		opacity_1: 1,
		opacity_2: 1,
		opacity_3: 1,
		dx_1: 5,
		dx_2: -5,
		dx_3: 0,
		dy_1: 10,
		dy_2: 1,
		dy_3: 5
	}
]
const ZigiFilters = [
	{
		freq: "0.02",
		octv: 3,
		seed: 0,
		scale: 6
	}, {
		freq: "0.02",
		octv: 3,
		seed: 1,
		scale: 8
	}, {
		freq: "0.02",
		octv: 3,
		seed: 1.5,
		scale: 8
	}, {
		freq: "0.18",
		octv: 3,
		seed: 2,
		scale: 8
	}, {
		freq: "0.12",
		octv: 3,
		seed: 2.5,
		scale: 8
	}
]
const Symbols = () => {
	return (
		<svg className="symbols">
			<defs>
				{ColorFilters.map(obj => {
					return (<BoxesFilter key={obj.name} id={obj.name} item={obj}/>)
				})}
				{ZigiFilters.map((filter, i) => {
					const id = 's' + i;
					const {freq, octv, seed, scale} = filter;
					return (<ZigiFilter key={i} id={id} freq={freq} octv={octv} seed={seed} scale={scale}/>)
				})}
				<filter id="grayscale">
					<feColorMatrix type="matrix" values="0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0"/>
				</filter>
			</defs>
		</svg>
	)
}
export default Symbols;