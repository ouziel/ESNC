import React, {PropTypes} from 'react';
import loaderTL from 'animations/loaderTL';
import mainTimeline from 'animations/mainTimeline';
export default class Loader extends React.Component {
	constructor(props) {
		super(props);
	}
	componentDidMount() {
		const loaderNode = this.refs.loader;
		mainTimeline.addTimeline({name: 'loadingBar', timeline: loaderTL(loaderNode)});
	}
	render() {
		return (
			<div className="loader" ref="loader">
				<div id="bar"></div>
				<div id="bg"></div>
			</div>
		);
	}
}
Loader.propTypes = {};