import React, {PropTypes} from 'react';
import Logo from 'components/Logo';
import {attachFormEvents} from 'util/sendForm';
import {formHandler} from 'util';
import {refs} from 'config';;
export default class Contact extends React.Component {
	constructor(props) {
		super(props);
	}
	componentDidMount() {
		const {form, btn, contact, email} = this.refs;
		refs.update(this.refs);
		refs.printAll();
		attachFormEvents({form, btn});
	}
	render() {
		return (
			<div>
				<main className="contact_page">
					<div className="float">
						<div className="contact">
							<span ref="contact">CONTACT</span>
						</div>
						<div className="links">
							<div className="link_item">
								<a href="mailto:hey@esnc.io">
									<svg fill="#000000" height="24" viewBox="0 0 24 24" width="24">
										<circle cx="50%" cy="50%" r="80%"></circle>
										<path ref="email_path" d="M20 4H4c-1.1 0-1.99.9-1.99 2L2 18c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 4l-8 5-8-5V6l8 5 8-5v2z"/>
										<path ref="done_path" className="hidden" d="M9 16.2L4.8 12l-1.4 1.4L9 19 21 7l-1.4-1.4L9 16.2z"/>
									</svg>
								</a>
								<span className="detail" ref="detail">hey
									<span className="mark">@</span>
									esnc.io
								</span>
							</div>
						</div>
					</div>
					<div className="float form_box">
						<form ref="form">
							<div>
								<input type="text" name="fname" placeholder="Name"/>
							</div>
							<div>
								<input type="tel" name="tel" placeholder="Tel"/>
							</div>
							<div>
								<input type="email" name="email" ref="email" placeholder="Email" required/>
							</div>
							<div>
								<input type="text" name="company" placeholder="Company"/>
							</div>
							<div>
								<input type="submit" hidden/>
								<button ref="btn">DO IT</button>
								<span className="button_text">DO IT</span>
							</div>
						</form>
					</div>
				</main>
			</div>
		)
	}
}