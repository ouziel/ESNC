import React, {PropTypes} from 'react';
import Banner from 'components/Banner';
import Slogan from 'components/Slogan';
const Home = () => {
	return (
		<div className="Page">
			<main className="homepage">
				<Slogan/>
				<Banner/>
			</main>
		</div>
	)
}
export default Home