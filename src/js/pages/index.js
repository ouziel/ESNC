import React, {PropTypes} from 'react';
import Home from './Home';
import Contact from './Contact';
import Logo from 'components/Logo';
export {Home, Contact};