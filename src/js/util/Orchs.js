import {TweenMax, TimelineMax} from 'gsap';
import {log} from 'util';
class Orchs {
	constructor() {
		this.timelines = {};
		this.sequences = {};
	}
	addTimeline({timeline, name}) {
		if (timeline && name) {
			let a = Object.assign(this.timelines, {[name]: timeline})
			log(`added TimeLine ---> ${name}`);
		}
	}
	addSequence({name, timelines}) {
		if (name && timelines) {
			try {
				Object.assign(this.sequences, {[name]: timelines});
				log(`added Sequence ---> ${name}, with timelines: ${timelines.join(',')}`);
			} catch (error) {
				console.log(error);
			}
		}
	}
	runTimeline({timeline, name}) {
		if (timeline && name) {
			timeline.play();
			log(`running timeline ---> ${name}`);
		}
	}
	runSequence(name) {
		if (name) {
			const timelines = this.sequences[name];
			const tl = new TimelineMax();
			tl.stop();
			tl.add(name);
			log('running seq ' + name)
			timelines.forEach(timeline => {
				if (timeline && this.timelines[timeline]) {
					tl.add(timeline);
					tl.add(this.timelines[timeline], timeline);
				}
			})
			tl.play();
		}
	}
	getTls() {
		return this.timelines;
	}
	getSeqs() {
		return this.sequences;
	}
}
export default Orchs;