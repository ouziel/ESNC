import {production} from 'config';
const log = (x, error = false) => (!production)
	? (error)
		? console.error('[ERROR]: ', x)
		: console.log('[LOG]: ', x)
	: null;
export default log;