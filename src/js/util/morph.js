import {TweenMax, TimelineMax} from 'gsap';
import $ from 'jquery';
import MorphSVGPlugin from 'gsap/src/uncompressed/plugins/MorphSVGPlugin.js';
function morph(...args) {
	// repeat
	const tl = new TimelineMax({
			onComplete: () => loop(),
			// paused: true
			// repeat: -1,
			// yoyo: true
		}),
		// not using banner for now
		banner = args[0],
		svg = banner.querySelector('svg'),
		el = args[1];
	function loop() {
		// morph, skip self
		args.reduce((a, b, index) => {
			if (index) {
				tl.to(el, 2, {
					morphSVG: {
						shape: b,
						shapeIndex: 'auto'
					},
					ease: Power4.easeInOut,
					onComplete() {
						// $(svg).addClass('foo');
					}
				})
				return b;
			}
		}, 2)
	}
	// anim();
	return tl;
}
export default morph;