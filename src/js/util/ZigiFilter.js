import React, {PropTypes} from 'react';
const ZigiFilter = ({id, freq, octv, seed, scale}) => {
	return (
		<filter id={id}>
			<feTurbulence baseFrequency={freq} numOctaves={octv} result="noise" seed={seed}/>
			<feDisplacementMap id="displacement" in="SourceGraphic" in2="noise" scale={scale}/>
		</filter>
	);
}
export default ZigiFilter;