import firebase from 'firebase';
function updateDB(data) {
	let postKey = firebase
		.database()
		.ref()
		.child('posts')
		.push()
		.key;
	const updates = {};
	updates['/posts/' + postKey] = data;
	return firebase
		.database()
		.ref()
		.update(updates);
}
export default updateDB;