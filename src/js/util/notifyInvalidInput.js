function notifyInvalidInput(elm) {
	elm.style.borderBottom = '1px solid red';
	elm.focus();
}
export default notifyInvalidInput;