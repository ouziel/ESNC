import imgBASE64 from './image';
const loadPixelData = (target) => {
	if (!target)
		return false;
	var png = new Image();
	png.onload = drawScene;
	png.src = imgBASE64;
	function drawScene() {
		const canvas = document.createElement('canvas'),
			ctx = canvas.getContext('2d');
		target.appendChild(canvas);
		const W = png.width * 3,
			H = png.height * 3;
		canvas.width = 100;
		canvas.height = 200;
		ctx.drawImage(png, 0, 0);
		const data = ctx.getImageData(0, 0, png.width, png.height);
		ctx.clearRect(0, 0, canvas.width, canvas.height);
		ctx.fillStyle = "white";
		const particles = [];
		for (var y = 0, y2 = data.height; y < y2; y++) {
			for (var x = 0, x2 = data.width; x < x2; x++) {
				if (data.data[(x * 4 + y * 4 * data.width) + 3] > 128) {
					var particle = {
						x0: x,
						y0: y,
						x1: 0,
						y1: png.height,
						speed: Math.random() * 4 + 2
					};
					TweenMax.to(particle, particle.speed, {
						x1: particle.x0,
						y1: particle.y0,
						delay: y / 30,
						ease: Elastic.easeOut
					});
					particles.push(particle);
				}
			}
		}
		requestAnimationFrame(render);
		function render() {
			requestAnimationFrame(render);
			ctx.clearRect(0, 0, canvas.width, canvas.height);
			for (var i = 0, j = particles.length; i < j; i++) {
				var particle = particles[i];
				ctx.fillRect(particle.x1 * 3, particle.y1 * 3, 2, 2);
			}
		};
	}
}
export default loadPixelData;