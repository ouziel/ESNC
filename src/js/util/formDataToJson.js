import $ from 'jquery';
function formDataToJson(form) {
	const data = $(form)
		.serializeArray()
		.reduce((a, x) => {
			a[x.name] = x.value;
			return a;
		}, {});
	return data;
}
export default formDataToJson;