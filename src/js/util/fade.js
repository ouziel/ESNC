import {TimelineMax} from 'gsap';
const fade = (el, fadeIn = false) => {
	const tl = new TimelineMax();
	tl.to(el, .2, {
		opacity: (fadeIn)
			? 1
			: 0
	})
}
export default fade;