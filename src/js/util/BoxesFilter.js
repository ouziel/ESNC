import React, {PropTypes} from 'react';
const BoxesFilter = ({
	id,
	item: {
		opacity_1,
		opacity_2,
		opacity_3,
		dx_1,
		dy_1,
		dx_2,
		dy_2,
		dx_3,
		dy_3,
		colors: [color1, color2, color3]
	}
}) => {
	return (
		<filter id={id}>
			<feFlood floodColor={color1} floodOpacity={opacity_1} result="b1"/>
			<feFlood floodColor={color2} floodOpacity={opacity_2} result="b2"/>
			<feFlood floodColor={color3} floodOpacity={opacity_3} result="b3"/>
			<feComposite operator="in" in="b1" in2="SourceAlpha" result="b1_text"/>
			<feComposite operator="in" in="b2" in2="SourceAlpha" result="b2_text"/>
			<feComposite operator="in" in="b3" in2="SourceAlpha" result="b3_text"/>
			<feOffset in="b1_text" dx={dx_1} dy={dy_1} result="b1_text_out"/>
			<feOffset in="b2_text" dx={dx_2} dy={dy_2} result="b2_text_out"/>
			<feOffset in="b3_text" dx={dx_3} dy={dy_3} result="b3_text_out"/>
			<feMerge>
				<feMergeNode in="b1_text_out"/>
				<feMergeNode in="b2_text_out"/>
				<feMergeNode in="b3_text_out"/>
			</feMerge>
		</filter>
	)
}
export default BoxesFilter;