import $ from 'jquery';
import {refs} from 'config';
import ScrambleTextPlugin from 'gsap/src/uncompressed/plugins/ScrambleTextPlugin.js';
import TextPlugin from 'gsap/src/uncompressed/plugins/TextPlugin.js';
import {TweenMax, TimelineMax} from 'gsap';
import {log} from 'util';
import {validateEmail, notifyInvalidInput, updateDB, formDataToJson} from 'util';
function attachFormEvents({btn, form}) {
	$(form).off('submit');
	$(btn).off('click');
	$(form).on('submit', formHandler);
	$(btn).on('click', formHandler);
}
function toggleSubmitBtnState() {
	const {btn} = refs.data;
	const disabled = $(btn).attr('disabled');
	if (disabled) {
		$(btn).removeAttr('disabled', '');
	} else {
		$(btn).attr('disabled', '');
	}
}
function formHandler(e) {
	e.preventDefault();
	const {form, contact, btn, email} = refs.data;
	const data = formDataToJson(form);
	if (!data.email || !validateEmail(email)) {
		notifyInvalidInput(email);
	} else {
		const didUpdate = updateDB(data);
		toggleSubmitBtnState();
		didUpdate.then(() => completeFormInteraction()).catch(err => {
			log(error);
			toggleSubmitBtnState();
		});
	}
}
function completeFormInteraction() {
	const {form, email_path, done_path, detail, contact} = refs.data;
	const tl = new TimelineMax();
	tl
		.to(form, 1, {opacity: 0})
		.to(contact, .5, {
			text: 'THANKS!',
			ease: Sine.easeInOut
		}, "-=0.3")
		.to(email_path, 1.5, {
			morphSVG: {
				shape: done_path,
				shapeIndex: 'auto'
			},
			ease: Power4.easeIn
		}, "-=1")
		.to(detail, .5, {
			text: "We'll be in touch"
		}, "-=1")
}
export {attachFormEvents};
export default formHandler;