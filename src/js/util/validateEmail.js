const validateEmail = (email) => email
	.value
	.includes('@');
export default validateEmail;