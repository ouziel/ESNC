const addClass = (el, cls) => {
	if (el && cls) {
		el
			.classList
			.add(cls);
	}
}
export default addClass;